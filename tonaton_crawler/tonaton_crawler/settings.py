import scrapy_mongodb

DEBUG = True
BOT_NAME = 'tonaton_crawler'

SPIDER_MODULES = ['tonaton_crawler.spiders']
NEWSPIDER_MODULE = 'tonaton_crawler.spiders'

ITEM_PIPELINES = [
	'scrapy_mongodb.MongoDBPipeline',
]

MONGODB_URI = 'mongodb://localhost:27017'
MONGODB_DATABASE = 'tonaton'
MONGODB_COLLECTION = 'contact_details'
