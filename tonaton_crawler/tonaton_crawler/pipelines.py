from scrapy.exceptions import DropItem

class DuplicateFilterPipeline(object):

	def __init__(self):
		self.links_visisted = set()

    def process_item(self, item, spider):
        if item['link'] in self.links_visisted:
        	raise DropItem("Duplicate item found %s" % item)
        else:
        	self.links_visisted.add(item['link'])
        	return item

