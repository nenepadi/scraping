import scrapy

class TonatonCrawlerItem(scrapy.Item):
	name = scrapy.Field()
	contact = scrapy.Field()
	location = scrapy.Field()
	category = scrapy.Field()
	link = scrapy.Field()