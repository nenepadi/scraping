import scrapy.spider
from scrapy.selector import Selector
from tonaton_crawler.items import TonatonCrawlerItem
import urlparse
from scrapy.http.request import Request

class TonatonCrawlerSpider(scrapy.Spider):
	name = "tonaton_crawl"
	allowed_domains = ["tonaton.com"]
	start_urls = [
		"http://tonaton.com/en/ads-in-ghana",
	]

	def parse(self, response):
		sel = Selector(response)
		listings = sel.xpath('//ul[@id="item-rows"]/li')
		links = []


		for listing in listings:
			link = listing.xpath('//div[@class="title"]/h2/a/@href').extract()

			links = link

		for clink in links:
			item = TonatonCrawlerItem()
			item['link'] = clink
			yield Request(urlparse.urljoin(response.url, clink), meta={'item':item},callback=self.parse_listing_page)

		for page in range(2, 3673):
			next_page = "?page=%s" % page
			yield Request(urlparse.urljoin(response.url, next_page), self.parse)
		# next_page = sel.select('//div[@class="page"]/span/a[@href]').extract()[0]
		# if next_page:
		# 	yield Request(urlparse.urljoin(response.url, next_page), self.parse)



	def parse_listing_page(self,response):
		hxs = Selector(response)
		item = response.request.meta['item']
		name = hxs.xpath('//div[@class="item-meta"]/a[@href]/text()').extract()
		if name:
			item["name"] = name[0]

		item['contact'] = hxs.xpath('//div[@class="number"]/text()').extract()
		locate_cat = hxs.xpath('//div[@class="attr"]/span[@class="value"]/a[@href]/text()').extract()

		if len(locate_cat) == 2:
			item['category'] = locate_cat[0]
			item['location'] = locate_cat[1]
		else:
			item['location'] = locate_cat
			 
		yield item